---
typora-root-url: ../beetl3.0
---

# Beetl3 高速模板引擎

iBeetl.com © 2011 ~ 2019，国内流行模板引擎，



#### 功能

http://ibeetl.com/guide/#beetl

* 动态页面生成
* 静态页面生成
* 代码生成
* 通过生成XML文本中间格式间接生成PDF，WORD等格式
* 短信，微信等模板内容生成
* 脚本引擎
* 规则引擎



#### 介绍

Beetl  ['biːtl]  3.0,从https://github.com/javamonkey/beetl2.0/ 迁移过来

Beetl的成长离不开以下人员的帮助（排名不分先后）：

- [作死模式](javascript:;)
- [一粟蜉蝣](javascript:;)
- [逝水fox](javascript:;)
- [kraken](javascript:;)
- [西安玛雅牛](javascript:;)
- [级?!](javascript:;)
- [orangetys](javascript:;)
- [Oo不懂oO](javascript:;)
- [原上一颗草](javascript:;)
- [龙图腾飞](javascript:;)
- [nutz](javascript:;)
- [天方地圆](javascript:;)
- [九月](javascript:;)
- [Daemons](javascript:;)
- [Gavin.King](javascript:;)
- [Sue](javascript:;)
- [Zhoupan](javascript:;)
- [woate](javascript:;)
- [fitz](javascript:;)
- [darren](http://darren.ink/)
- [zqq](javascript:;)
- [ 醉引花眠](javascript:;)




QQ交流群：636321496,219324263

Beetl官网   [ibeetl.com](ibeetl.com) 

Beetl社区：[bbs.ibeetl.com](http://42.96.162.109/bbs/bbs/index/1.html)




#### 性能

<https://github.com/fizzed/template-benchmark>

![p1](/doc/resources/p1.jpg) 


#### 支持

任何企业和个人都可以免费使用，并能免费得到社区，论坛，QQ群和作者的免费技术支持。以下情况需要收费技术支持，详情可联系微信（lliijjzz），备注“商业技术支持”

* 任何公开申明了996工作制度得企业，将收取7996元/年 的费用
* 想获得商业技术支持，如培训，技术咨询，定制，售后等，可根据公司规模收取1000-10000元 年费

